var app = angular.module('myApp', []);
app.controller('userController', function($scope){
	console.log("Controller loaded");
	$scope.users = [{
		firstName: "Bill",
		lastName: "Gates",
		isFemale: false,
		company: "MS",
		image: "pics/bill.jpg",
		comments: [{
			author: "Sung",
			body: "Good guy McFaggyPants"
		},{
			author: "Harsh",
			body: "Nerd!"
		}]
		},{
		firstName: "Steve",
		lastName: "Jobs",
		isFemale: true,
		company: "Apple Inc",
		image: "pics/steve.jpg",
		comments: []
		},{
		firstName: "Warren",
		lastName: "Buffet",
		isFemale: false,
		company: "Don't call me at home",
		image: "pics/warren.jpg",
		comments: []
		}]
});

app.controller('SectionController', function($scope){
	$scope.tab = 1;
	$scope.isSelected = function(tabToCheck){
		return $scope.tab == tabToCheck
	}
	$scope.selectedTab = function(seltab){
		$scope.tab = seltab;
	}
})

app.controller('FormController', function($scope){
	$scope.comment = {}
	$scope.formSubmit = function(user){
		user.comments.push($scope.comment)
		$scope.comment = {};
	}
})

app.filter('dopeFilter', function(){
	return function(str, isFemale){
		if(isFemale){
			return 'Ms. ' + str;
		}
		return 'Mr. ' + str;
	}
})